﻿namespace libCalculadoraUC
{
    public class Operacion
    {
        private int num1, num2;
        public int Num1 { get => Num1; set => Num1 = value; }
        public int Num2 { get => num2; set => num2 = value; }

        public int Suma()
        {
            return num1 + num2;
        }
        public int Resta()
        {
            return num1 - num2;
        }
    }
}